<?php

namespace App\Traits;

use App\Scopes\PublishedScope;

trait PublishedTrait
{
    /** @var string */
    private $publishedColumnName = 'is_published';

    public static function bootPublishedTrait(): void
	{
		static::addGlobalScope(new PublishedScope);
	}

    public function getQualifiedPublishedColumn(): string
    {
        return $this->getTable() . '.' . $this->publishedColumnName;
    }

    public function checkIfColumnExists(): bool
    {
        return $this->getConnection()
           ->getSchemaBuilder()
           ->hasColumn($this->getTable(), $this->publishedColumnName);
    }
}

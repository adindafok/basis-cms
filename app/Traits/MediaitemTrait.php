<?php

namespace App\Traits;

use App\Models\Mediaitem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Illuminate\Support\Str;

trait MediaitemTrait
{
    public function saveMediaitem(UploadedFile $uploadedFile): Mediaitem
    {
        $originalName = $uploadedFile->getClientOriginalName();
        $originalName = pathinfo($originalName, PATHINFO_FILENAME);
        $type = $uploadedFile->getClientOriginalExtension();
        $fileName = Str::uuid();
        $path = 'uploads/' . now()->format('Y') . '/' . now()->format('m') . '/' . $fileName . '.' . $type;

        $mediaitem = Mediaitem::updateOrCreate([
            'filename' => $fileName,
            'original_filename' => $originalName,
            'type' => $type,
            'path' => $path,
        ]);

        try {
            $uploadedFile->move(storage_path('app/public/uploads/' . now()->format('Y') . '/' . now()->format('m')), $path);
        } catch (FileException $e) {
            abort(403);
        }

        return $mediaitem;
    }
}

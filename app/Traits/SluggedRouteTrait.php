<?php

namespace App\Traits;

trait SluggedRouteTrait
{
    public function getRouteKeyName(): string
    {
        if (request()->segment(1) !== 'admin' && $this->checkIfModelHasColumnSlug()) {
            return 'slug';
        }

        return 'id';
    }

    private function checkIfModelHasColumnSlug(): bool
    {
        return $this->getConnection()
           ->getSchemaBuilder()
           ->hasColumn($this->getTable(), 'slug');
    }
}

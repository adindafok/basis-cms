<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PublishedScope implements Scope
{
    CONST IS_PUBLISHED = 1;

    public function apply(Builder $builder, Model $model): void
    {
        if (request()->segment(1) !== 'admin' && $model->checkIfColumnExists()) {
            $column = $model->getQualifiedPublishedColumn();

            $builder->where($column, '=', SELF::IS_PUBLISHED);
        }
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public function build(): self
    {
        return $this
            ->to(config('mail.from.address'))
            ->subject(trans('emailtemplates.to-owner.subject'))
            ->view('emailtemplates.to-owner');
    }
}

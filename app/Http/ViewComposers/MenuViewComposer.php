<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Menu;

class MenuViewComposer
{
    public function compose(View $view)
    {
        $view
            ->withMainMenu(Menu::where('developer_label', 'main')->firstOrFail())
            ->withFooterMenu(Menu::where('developer_label', 'footer')->firstOrFail());
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Redirect;

class CheckRedirects
{
    /**
     * Handle an incoming request.
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $fromString = str_replace(url('/'), '', $request->getUri());
        $redirect = Redirect::where('from', $fromString);

        if (empty($redirect->count())) {
            return $next($request);
        }

        $redirect = $redirect->first();

        if (! Str::contains($redirect->to, url('/'))) {
            return redirect()->away($redirect->to);
        }

        return redirect($redirect->to);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\CrudController;
use Illuminate\Database\Eloquent\Model;
use App\Models\StaticFormSubmit;
use Illuminate\View\View;

class StaticFormSubmitController extends CrudController
{
    public function __construct()
    {
        $this->singularModelName = 'static_form_submit';
        $this->pluralModelName = 'static_form_submits';
        $this->modelClass = StaticFormSubmit::class;
    }
}

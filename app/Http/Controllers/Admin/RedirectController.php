<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\CrudController;
use App\Models\Redirect;
use App\Http\Requests\RedirectRequest;

class RedirectController extends CrudController
{
    public function __construct()
    {
        $this->singularModelName = 'redirect';
        $this->pluralModelName = 'redirects';
        $this->modelClass = Redirect::class;
        $this->requestForm = RedirectRequest::class;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;

class CrudController extends Controller
{
    /** @var Model */
    protected $modelClass;

    /** FormRequest */
    protected $requestForm;

    /** @var string */
    protected $singularModelName;

    /** @var string */
    protected $pluralModelName;

    public function index(): View
    {
        return view('admin.' . $this->singularModelName . '.index')->withItems($this->modelClass::orderBy('updated_at', 'desc')->get());
    }

    public function create(): View
    {
        return view('admin.create')
            ->withModel($this->modelClass)
            ->withAction(route('admin.' . $this->pluralModelName . '.store'));
    }

    public function store(Request $request): RedirectResponse
    {
        $request = app($this->requestForm);

        $this->modelClass::updateOrCreate($request->validated());

        return redirect()->route('admin.' . $this->pluralModelName . '.index');
    }

    public function show(int $id): View
    {
        return view('admin.show')->withEntity($this->modelClass::findOrFail($id));
    }

    public function preview(int $id): View
    {
        return view('admin.' . $this->singularModelName . '.preview')->withEntity($this->modelClass::findOrFail($id));
    }

    public function edit(int $id): View
    {
        $entity = $this->modelClass::findOrFail($id);

        return view('admin.edit')
            ->withEntity($entity)
            ->withAction(route('admin.' . $this->pluralModelName . '.update', [$this->singularModelName => $entity]));
    }

    public function update(Request $request, int $id): RedirectResponse
    {
        $request = app($this->requestForm);

        $this->modelClass::findOrFail($id)->update($request->validated());

        return redirect()->route('admin.' . $this->pluralModelName . '.index');
    }

    public function destroy(int $id): JsonResponse
    {
        $this->modelClass::findOrFail($id)->delete();

        return response()->json([
            'message' => trans('admin.general.deleting-success'),
        ]);
    }

    public function submittedForms(int $id): View
    {
        $entity = $this->modelClass::findOrFail($id);

        return view('admin.static_form_submit.index')->withEntity($entity);
    }

    public function saveOrder(Request $request): JsonResponse
    {
        foreach($request->all() as $id => $order) {
            $entity = $this->modelClass::findOrFail($id);

            $entity->order = $order;

            $entity->save();
        }

        return response()->json([
            'message' => trans('admin.general.saving-success'),
        ]);
    }
}

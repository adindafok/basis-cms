<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\CrudController;
use Illuminate\Database\Eloquent\Model;
use App\Models\Mediaitem;
use App\Http\Requests\MediaitemRequest;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Traits\MediaitemTrait;

class MediaitemController extends CrudController
{
    use MediaitemTrait;

    /** @var int */
    const TOTAL_UPLOADED_CKEDITOR = 1;

    public function __construct()
    {
        $this->singularModelName = 'mediaitem';
        $this->pluralModelName = 'mediaitems';
        $this->modelClass = Mediaitem::class;
        $this->requestForm = MediaitemRequest::class;
    }

    public function uploadCKEditor(MediaitemRequest $request): JsonResponse
    {
        $enableDebugMode = config('ckfinder.debug');

        $mediaitem = $this->saveMediaitem($request->file('upload'));

        $url = asset('storage/' . $mediaitem->path);

        return response()->json([
            'fileName' => $mediaitem->original_filename,
            'uploaded' => SELF::TOTAL_UPLOADED_CKEDITOR,
            'url' => $url,
        ]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\CrudController;
use App\Http\Requests\PageRequest;
use App\Http\Requests\MenuitemRequest;
use Illuminate\Database\Eloquent\Model;
use App\Models\Page;
use App\Models\Menu;
use App\Models\Pivots\Menuitem;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PageController extends CrudController
{
    public function __construct()
    {
        $this->singularModelName = 'page';
        $this->pluralModelName = 'pages';
        $this->modelClass = Page::class;
        $this->requestForm = PageRequest::class;
    }

    public function index(): View
    {
        return view('admin.' . $this->singularModelName . '.index')->withMenus(Menu::orderBy('order')->get());
    }

    public function storeExtended(PageRequest $request): RedirectResponse
    {
        $page = $this->modelClass::updateOrCreate($request->validated());

        $menuitemRequest = app(MenuitemRequest::class);

        $this->syncWithMenus($page, $menuitemRequest->get('menu_ids'));
        $page->static_forms()->sync($request->get('static_form_ids'));

        return redirect()->route('admin.' . $this->pluralModelName . '.index');
    }

    public function updateExtended(Pagerequest $request, int $id): RedirectResponse
    {
        $page = $this->modelClass::findOrFail($id);
        $page->update($request->validated());
        $page->save();

        $menuitemRequest = app(MenuitemRequest::class);

        $this->syncWithMenus($page, $menuitemRequest->get('menu_ids'));
        $page->static_forms()->sync($request->get('static_form_ids'));

        return redirect()->route('admin.' . $this->pluralModelName . '.index');
    }

    private function syncWithMenus(Page $page, array $menusIds)
    {
        $pivotData = [];

        foreach ($menusIds as $menuId) {
            $menuitem =  Menuitem::where('menu_id', $menuId)->where('page_id', $page->id)->first();

            if (! empty($menuitem)) {
                $order = $menuitem->order;
            }

            $pivotData[$menuId] = [];
            $pivotData[$menuId]['link'] = '/' . $page->slug;
            $pivotData[$menuId]['order'] = $order ?? Menuitem::where('menu_id', $menuId)->count();
        }

        $page->menus()->sync($pivotData);
    }
}

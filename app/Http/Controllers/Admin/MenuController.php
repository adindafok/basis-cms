<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\CrudController;
use Illuminate\Database\Eloquent\Model;
use App\Models\Menu;
use Illuminate\View\View;
use App\Http\Requests\MenuRequest;

class MenuController extends CrudController
{
    public function __construct()
    {
        $this->singularModelName = 'menu';
        $this->pluralModelName = 'menus';
        $this->modelClass = Menu::class;
        $this->requestForm = MenuRequest::class;
    }

    public function index(): View
    {
        return view('admin.' . $this->singularModelName . '.index')->withItems($this->modelClass::orderBy('order')->get());
    }
}

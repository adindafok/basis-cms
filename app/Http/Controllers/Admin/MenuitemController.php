<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\CrudController;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pivots\Menuitem;
use Illuminate\View\View;

class MenuitemController extends CrudController
{
    /** @var Model */
    protected $modelClass;

    /** @var string */
    protected $singularModelName;

    /** @var string */
    protected $pluralModelName;

    public function __construct()
    {
        $this->singularModelName = 'page';
        $this->pluralModelName = 'pages';
        $this->modelClass = Menuitem::class;
    }
}

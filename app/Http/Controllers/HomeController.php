<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Page;

class HomeController extends Controller
{
    public function show(): View
    {
        return view('home')->withPage(Page::where('developer_label', 'home')->firstOrFail());
    }
}

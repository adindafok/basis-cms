<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use Illuminate\Http\RedirectResponse;
use App\Models\StaticForm;
use App\Models\StaticFormSubmit;
use App\Mail\ContactMail;
use Illuminate\View\View;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
    * @param $formable mixed
    */
    public function store(ContactRequest $request, StaticForm $form): RedirectResponse
    {
        $form->submits()->create([
            'static_formable_id' => $request->get('static_formable_id'),
            'static_formable_type' => $request->get('static_formable_type'),
            'response' => json_encode($request->validated()),
        ]);

        try {
            Mail::send(new ContactMail());
        } catch (Exception $e) {
            mail(config('mail.webbuilder'), 'log: error contactform', $e->getMessage());

            return redirect()
                ->back()
                ->withErrors(['mail-not-send' => trans('emailtemplates.to-owner.error-message')]);
        }

        return redirect()
            ->back()
            ->withSuccessMessage(trans('emailtemplates.to-owner.success-message'));
    }
}

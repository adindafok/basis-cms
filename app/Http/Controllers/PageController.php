<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Page;

class PageController extends Controller
{
    public function show(Page $page): View
    {
        return view('home')->withPage($page);
    }
}

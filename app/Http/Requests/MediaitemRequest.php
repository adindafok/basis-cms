<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MediaitemRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'upload' => 'required|file',
        ];
    }
}

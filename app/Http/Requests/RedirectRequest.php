<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RedirectRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'from' => 'required|unique:redirects,from,' . $this->id,
            'to' => 'required',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    public function authorize(): bool
    {
        if (! empty(request()->get('extra_field'))) {
            return false;
        }

        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'content' => 'required',
            'static_formable_id' => 'required',
            'static_formable_type' => 'required'
        ];
    }
}

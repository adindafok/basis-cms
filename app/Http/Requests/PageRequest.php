<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'developer_label' => 'nullable',
            'is_published' => 'boolean',
        ];
    }
}

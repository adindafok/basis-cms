<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuitemRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'menu_ids' => 'required',
        ];
    }
}

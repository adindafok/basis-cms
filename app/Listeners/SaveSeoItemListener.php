<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\SaveModelEvent;
use Illuminate\Support\Str;
use App\Models\Page;
use App\Models\Seo;
use BadMethodCallException;

class SaveSeoItemListener
{
    public function handle(SaveModelEvent $event): void
    {
        try {
            $event->entity->seo();
        } catch (BadMethodCallException $e) {
            return;
        }

        $request = request()->get('seo') ?? [];

        $seoData = [
            'title' => $request['title'] ?? $event->entity->title,
            'description' => strip_tags($request['description'] ?? Str::limit($event->entity->content, 155, '...')),
            'index' => $request['index'] ?? 'index, follow',
        ];

        $event->entity->seo()->updateOrCreate(['id' => $request['id'] ?? null], $seoData);
    }

}

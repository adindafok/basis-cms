<?php

namespace CKSource\CKFinder\Plugin\SaveMediaitem;

use CKSource\CKFinder\CKFinder;
use CKSource\CKFinder\Event\BeforeCommandEvent;
use CKSource\CKFinder\Event\CKFinderEvent;
use CKSource\CKFinder\Image;
use CKSource\CKFinder\Plugin\PluginInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Traits\MediaitemTrait;

class SaveMediaitem implements PluginInterface, EventSubscriberInterface
{
    use MediaitemTrait;

    public function setContainer(CKFinder $app) {}

    public function getDefaultConfig() {
        return [];
    }

    public static function getSubscribedEvents()
    {
        return [
            CKFinderEvent::BEFORE_COMMAND_FILE_UPLOAD => 'handleBeforeFileUpload',
            CKFinderEvent::BEFORE_COMMAND_QUICK_UPLOAD => 'handleBeforeFileUpload'
        ];
    }

    public function handleBeforeFileUpload(BeforeCommandEvent $event) {
        $request = $event->getRequest();

        /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('upload');

        if ($uploadedFile && $uploadedFile->isValid()) {
            $mediaitem = $this->saveMediaitem($uploadedFile);
        }

        $request->query->set('type', $uploadedFile->getClientOriginalExtension());
    }
}

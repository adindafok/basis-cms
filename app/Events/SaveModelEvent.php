<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
use App\Models\BaseModel;

class SaveModelEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var Request */
    public $request;

    /** @var BaseModel */
    public $entity;

    public function __construct(Request $request, BaseModel $entity)
    {
        $this->request = $request;
        $this->entity = $entity;
    }
}

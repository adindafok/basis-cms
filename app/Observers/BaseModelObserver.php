<?php

namespace App\Observers;

use App\Models\BaseModel;
use App\Events\SaveModelEvent;
use Illuminate\Http\Request;

class BaseModelObserver
{
    public function saved(BaseModel $entity)
    {
        event(new SaveModelEvent(request(), $entity));
    }
}

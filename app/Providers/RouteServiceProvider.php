<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
    * This namespace is applied to your controller routes.
    *
    * In addition, it is set as the URL generator's root namespace.
    *
    * @var string
    */
    protected $namespace = 'App\Http\Controllers';

    /**
    * The path to the "home" route for your application.
    *
    * @var string
    */
    public const HOME = '/admin/pages';

    /**
    * Define your route model bindings, pattern filters, etc.
    *
    * @return void
    */
    public function boot()
    {
        //

        parent::boot();
    }

    public function map(): void
    {
        $this->mapAuthRoutes();

        $this->mapAdminRoutes();

        $this->mapWebRoutes();
    }

    protected function mapWebRoutes(): void
    {
        Route::middleware(['web', 'redirect'])
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    protected function mapAuthRoutes(): void
    {
        Route::prefix('admin')
            ->name('admin.')
            ->middleware(['web'])
            ->namespace($this->namespace . '\Admin')
            ->group(base_path('routes/auth.php'));
    }

    protected function mapAdminRoutes(): void
    {
        Route::prefix('admin')
            ->name('admin.')
            ->middleware(['web', 'auth'])
            ->namespace($this->namespace . '\Admin')
            ->group(base_path('routes/admin.php'));
    }
}

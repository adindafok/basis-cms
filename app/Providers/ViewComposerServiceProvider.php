<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Http\ViewComposers\MenuViewComposer;

class ViewComposerServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        View::composer(['home'], MenuViewComposer::class);
    }
}

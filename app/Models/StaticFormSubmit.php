<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\StaticForm;

class StaticFormSubmit extends BaseModel
{
    /** @var array */
    protected $fillable = [
        'response',
        'static_formable_id',
        'static_formable_type',
    ];

    /** @var array */
    protected $casts = [
        'response' => 'array',
    ];

    public static function getFields(): array
    {
        return [
            [
                'name' => 'response',
                'type' => 'json',
                'label' => trans('admin.static_form_submits.fields.response'),
            ],
        ];
    }

    public function static_formable(): MorphTo
    {
        return $this->morphTo();
    }

    public function static_form(): BelongsTo
    {
        return $this->belongsTo(StaticForm::class);
    }
}

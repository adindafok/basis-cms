<?php

namespace App\Models\Pivots;

use App\Models\Page;
use App\Models\Menu;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Menuitem extends Pivot
{
    /** @var string */
    protected $table = 'menuitems';

    /** @var bool */
    public $incrementing = true;

    /** @var array */
    protected $fillable = [
        'parent_id',
        'order',
        'link',
        'menu_id',
        'page_id',
    ];

    public function page(): BelongsTo
    {
        return $this->belongsTo(Page::class);
    }

    public function menu(): BelongsTo
    {
        return $this->belongsTo(Menu::class);
    }
}

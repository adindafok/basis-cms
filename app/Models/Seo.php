<?php

namespace App\Models;

use App\Models\Page;
use App\Models\Pivots\Menuitem;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Seo extends BaseModel
{
    /** @var string */
    protected $table = 'seo';

    /** @var array */
    protected $fillable = [
        'title',
        'description',
        'index',
    ];

    public static function getFields(): array
    {
        return [
            [
                'name' => 'title',
                'type' => 'text',
                'label' => trans('admin.seo.fields.title'),
            ],
            [
                'name' => 'description',
                'type' => 'text',
                'label' => trans('admin.seo.fields.description'),
            ],
            [
                'name' => 'index',
                'type' => 'select_from_array',
                'label' => trans('admin.seo.fields.index'),
                'options' => [
                    'index, follow' => 'index, follow (default)',
                    'noindex, follow' => 'noindex, follow',
                    'index, nofollow' => 'index, nofollow',
                    'noindex, nofollow' => 'noindex, nofollow',
                ],
            ],
        ];
    }

    public function seoable(): MorphTo
    {
        return $this->morphTo();
    }
}

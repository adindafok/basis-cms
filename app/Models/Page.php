<?php

namespace App\Models;

use App\Models\Block;
use App\Models\Seo;
use App\Models\Menu;
use App\Models\Pivots\Menuitem;
use App\Models\StaticFormSubmit;
use App\Models\StaticForm;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Cviebrock\EloquentSluggable\Sluggable;

class Page extends BaseModel
{
    use Sluggable;

    /** @var array */
    protected $fillable = [
        'developer_label',
        'title',
        'content',
        'is_published',
        'slug',
    ];

    /** @var array */
    protected $casts = [
        'is_private' => 'boolean',
    ];

    public static function getFields(): array
    {
        return [
            [
                'name' => 'menu_ids[]',
                'type' => 'many-to-many',
                'options' => Menu::pluck('title', 'id'),
                'relation' => 'menus',
                'label' => trans('admin.pages.fields.menu_id'),
            ],
            [
                'name' => 'title',
                'type' => 'text',
                'label' => trans('admin.pages.fields.title'),
            ],
            [
                'name' => 'slug',
                'type' => 'hidden',
                'label' => trans('admin.pages.fields.slug'),
            ],
            [
                'name' => 'developer_label',
                'type' => 'text',
                'label' => trans('admin.pages.fields.developer_label'),
                'role' => 'super_admin',
            ],
            [
                'name' => 'is_published',
                'type' => 'boolean',
                'label' => trans('admin.pages.fields.is_published'),
            ],
            [
                'name' => 'content',
                'type' => 'editor',
                'label' => trans('admin.pages.fields.summary'),
            ],
            [
                'name' => 'seo',
                'type' => 'has-one',
                'label' => trans('admin.seo.title'),
                'relation' => 'seo',
                'fields' => Seo::getFields(),
            ],
            [
                'name' => 'static_form_ids[]',
                'type' => 'many-to-many',
                'options' => StaticForm::pluck('name', 'id'),
                'relation' => 'static_forms',
                'label' => trans('admin.pages.fields.static_forms'),
            ],
        ];
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function menuitems(): HasMany
    {
        return $this->hasMany(Menuitem::class)->orderBy('order');
    }

    public function menus(): BelongsToMany
    {
        return $this->belongsToMany(Menu::class, 'menuitems', 'page_id', 'menu_id')->using(Menuitem::class);
    }

    public function seo(): MorphOne
    {
        return $this->morphOne(Seo::class, 'seoable');
    }

    public function static_forms(): MorphToMany
    {
        return $this->morphToMany(StaticForm::class, 'static_formable');
    }

    public function submits(): MorphMany
    {
        return $this->morphMany(StaticFormSubmit::class, 'static_formable')->orderBy('created_at', 'desc');
    }
}

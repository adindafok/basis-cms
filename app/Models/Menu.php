<?php

namespace App\Models;

use App\Models\Page;
use App\Models\Pivots\Menuitem;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Menu extends BaseModel
{
    /** @var array */
    protected $fillable = [
        'developer_label',
        'title',
        'order',
    ];

    public static function getFields(): array
    {
        return [
            [
                'name' => 'title',
                'type' => 'text',
                'label' => trans('admin.menus.fields.title'),
            ],
            [
                'name' => 'developer_label',
                'type' => 'text',
                'label' => trans('admin.menus.fields.developer_label'),
                'role' => 'super_admin',
            ],
        ];
    }

    public function menuitems(): HasMany
    {
        return $this->hasMany(Menuitem::class)->orderBy('order');
    }

    public function pages(): BelongsToMany
    {
        return $this->belongsToMany(Page::class, 'menuitems', 'menu_id', 'page_id')->using(Menuitem::class);
    }
}

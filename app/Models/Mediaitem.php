<?php

namespace App\Models;

class Mediaitem extends BaseModel
{
    /** @var array */
    protected $fillable = [
        'original_filename',
        'filename',
        'path',
        'type',
    ];
}

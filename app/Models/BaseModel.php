<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\PublishedTrait;
use App\Traits\SluggedRouteTrait;
use App\Observers\BaseModelObserver;

class BaseModel extends Model
{
    use SoftDeletes;
    use PublishedTrait;
    use SluggedRouteTrait;

    protected static function boot(): void
    {
        parent::boot();

        self::observe(BaseModelObserver::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use App\Models\StaticFormSubmit;
use App\Models\Page;

class StaticForm extends BaseModel
{
    /** @var array */
    protected $fillable = [
        'name',
        'developer_label',
        'blade_template',
    ];

    public function submits(): HasMany
    {
        return $this->hasMany(StaticFormSubmit::class);
    }

    public function pages(): MorphToMany
    {
        return $this->morphedByMany(Page::class, 'static_formable');
    }
}

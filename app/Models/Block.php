<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\MorphTo;

class Block extends BaseModel
{
    /** @var array */
    protected $fillable = [
        'type',
        'content',
        'order',
    ];

    public function blockable(): MorphTo
    {
        return $this->morphTo();
    }
}

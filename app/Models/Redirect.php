<?php

namespace App\Models;

class Redirect extends BaseModel
{
    /** @var array */
    protected $fillable = [
        'from',
        'to',
    ];

    public static function getFields(): array
    {
        return [
            [
                'name' => 'from',
                'type' => 'text',
                'label' => trans('admin.redirects.fields.from'),
            ],
            [
                'name' => 'to',
                'type' => 'text',
                'label' => trans('admin.redirects.fields.to'),
            ],
        ];
    }
}

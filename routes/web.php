<?php

Route::get('/', 'HomeController@show')->name('home');
Route::get('admin', 'Admin\Auth\LoginController@login');

Route::post('contact/{form}', 'ContactController@store')->name('contact.store');

Route::any('{page}', 'PageController@show');

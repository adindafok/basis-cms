<?php

Route::get('/', 'PageController@index')->name('dashboard');

Route::delete('entity/{classname}/{id}', 'CrudController@delete')->name('delete-entity');

Route::resource('pages', 'PageController');
Route::prefix('pages')->name('pages.')->group(function() {
    Route::get('create/{menu}', 'PageController@createMenuItem')->name('create.menuitem');
    Route::post('store', 'PageController@storeExtended')->name('store');
    Route::put('update/{page}', 'PageController@updateExtended')->name('update');
    Route::get('static_form_submits/{page}', 'PageController@submittedForms')->name('submits');
});

Route::delete('menuitems/destroy/{menuitem}', 'MenuitemController@destroy')->name('menuitems.destroy');

Route::resource('menus', 'MenuController');
Route::resource('redirects', 'RedirectController');
Route::resource('static_form_submits', 'StaticFormSubmitController')->only(['show']);

Route::resource('mediaitems', 'MediaitemController');
Route::prefix('mediaitems')->group(function() {
    Route::any('ckeditor/upload', 'MediaitemController@uploadCKEditor')->name('ckeditor.upload');
});

Route::name('preview.')
    ->prefix('preview')
    ->group(function() {
        Route::get('page/{page}', 'PageController@preview')->name('pages');
});

Route::name('save-order.')
    ->prefix('save-order')
    ->group(function() {
        Route::post('menuitems', 'MenuitemController@saveOrder')->name('menuitems');
        Route::post('menus', 'MenuController@saveOrder')->name('menus');
});

## About this project

This is just a basic CMS made for personal purposes.

## How to make it work

- First clone the repository
- Copy the ".env.example" and rename it to ".env" and fill in the right credentials.
- composer install
- php artisan migrate
- php artisan storage:link
- php artisan db:seed
- npm install
- npm run dev

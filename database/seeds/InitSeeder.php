<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use App\Models\Page;
use App\Models\Pivots\Menuitem;
use App\Models\Menu;
use App\Models\User;
use App\Models\StaticForm;
use App\Models\Redirect;
use Spatie\Permission\Models\Role;

class InitSeeder extends Seeder
{
    public function run(): void
    {
        $this->createNavigation();

        $this->createRedirects();

        $this->createUsersWithRoles();
    }

    private function createNavigation(): void
    {
        $mainMenu = Menu::updateOrCreate([
            'developer_label' => 'main',
            'title' => trans('admin.seeder.menu.main'),
            'order' => 0,
        ]);

        $footerMenu = Menu::updateOrCreate([
            'developer_label' => 'footer',
            'title' => trans('admin.seeder.menu.footer'),
            'order' => 1,
        ]);

        $miscMenu = Menu::updateOrCreate([
            'title' => trans('admin.seeder.menu.misc'),
            'order' => 2,
        ]);

        $homePage = Page::updateOrCreate([
            'developer_label' => 'home',
            'title' => trans('admin.seeder.page.home'),
            'is_published' => 1,
        ], [
            'content' => '<p>This is the homepage</p>',
        ]);

        $contactPage = Page::updateOrCreate([
            'title' => trans('admin.seeder.page.contact'),
            'is_published' => 1,
        ], [
            'content' => '<p>This is the contact page.</p>'
        ]);

        $contactForm = StaticForm::updateOrCreate([
            'name' => trans('admin.seeder.form.contact'),
            'developer_label' => 'contact',
            'blade_template' => 'forms.contact',
        ]);

        $contactPage->static_forms()->sync([$contactForm->id]);

        Menuitem::updateOrCreate([
            'page_id' => $homePage->id,
            'menu_id' => $mainMenu->id,
            'link' => '/' . $homePage->slug,
            'order' => 0,
        ]);

        Menuitem::updateOrCreate([
            'page_id' => $contactPage->id,
            'menu_id' => $mainMenu->id,
            'link' => '/' . $contactPage->slug,
            'order' => 1,
        ]);
    }

    private function createRedirects(): void
    {
        Redirect::updateOrCreate([
            'from' => '/home',
            'to' => '/',
        ]);
    }

    private function createUsersWithRoles(): void
    {
        $roleSuperAdmin = Role::findOrCreate('super_admin');
        $roleAdmin = Role::findOrCreate('admin');

        $developerUser = User::updateOrCreate([
            'email' => 'adindafok@gmail.com',
        ], [
            'name' => 'Adinda',
            'password' => Hash::make('sadsalt55'),
        ]);

        $customerUser = User::updateOrCreate([
            'email' => 'adindafok+2@gmail.com',
        ], [
            'name' => 'Piet',
            'password' => Hash::make('dinopass'),
        ]);

        try {
            $roleSuperAdmin->users()->attach($developerUser);
            $roleAdmin->users()->attach($customerUser);
        } catch(QueryException $e) {
            $this->command->info('Users already have the right roles');
        }
    }
}

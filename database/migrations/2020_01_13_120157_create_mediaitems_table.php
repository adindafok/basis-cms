<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaitemsTable extends Migration
{
    public function up(): void
    {
        Schema::create('mediaitems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('filename');
            $table->string('original_filename');
            $table->string('path');
            $table->string('type');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('mediaitemables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mediaitem_id');
            $table->morphs('mediaitemable');

            $table->foreign('mediaitem_id')->references('id')->on('mediaitems');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('mediaitemables');
        Schema::dropIfExists('mediaitems');
    }
}

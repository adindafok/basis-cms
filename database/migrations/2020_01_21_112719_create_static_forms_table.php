<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticFormsTable extends Migration
{
    public function up(): void
    {
        Schema::create('static_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('developer_label');
            $table->string('blade_template');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('static_formables', function (Blueprint $table) {
            $table->unsignedBigInteger('static_form_id');
            $table->morphs('static_formable');

            $table->foreign('static_form_id')->references('id')->on('static_forms');
        });

        Schema::create('static_form_submits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('static_form_id');
            $table->morphs('static_formable', 'form_submit_formable_id_formable_type_index');
            $table->text('response');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('static_form_id')->references('id')->on('static_forms');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('static_form_submits');
        Schema::dropIfExists('static_formables');
        Schema::dropIfExists('static_forms');
    }
}

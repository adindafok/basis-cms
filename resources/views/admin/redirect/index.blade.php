@extends('admin.layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('admin.form-partials.submit', ['classes' => 'mb-4 d-none sticky-top bg-white'])

                <div class="card">
                    <h2 class="card-header font-weight-bold">
                        {{ trans('admin.redirects.title') }}
                        <a class="btn btn-sm btn-outline-success float-right" href="{{ route('admin.redirects.create') }}"><i class="fas fa-plus"></i> {{ trans('admin.general.add') }}</a>
                    </h2>

                    <ul class="list-group list-group-flush list-group-item-action">
                        @foreach($items as $redirect)
                            <li id="row-{{ $redirect->id }}" class="list-group-item" data-id="{{ $redirect->id }}">
                                <a href="{{ route('admin.redirects.edit', ['redirect' => $redirect]) }}">
                                    {{ $redirect->from }} -> {{ $redirect->to }}
                                </a>

                                <div class="btn-group btn-group-sm float-right" role="group" aria-label="actions">
                                    <button class="btn btn-sm btn-outline-danger js-delete-button" data-id="{{ $redirect->id }}"><i class="fas fa-trash"></i></button>
                                    <a href="{{ route('admin.redirects.edit', ['redirect' => $redirect]) }}" class="btn btn-sm btn-outline-primary"><i class="fas fa-pencil-alt"></i></a>
                                </div>

                                <div class="float-right mr-4 d-none" id="js-delete-container-{{ $redirect->id }}" role="group" aria-label="delete-actions">
                                    <em class="mr-4">{{ trans('admin.general.confirm-delete') }}</em>
                                    <button class="btn btn-sm btn-outline-warning js-delete-confirmed" data-url="{{ route('admin.redirects.destroy', ['redirect' => $redirect]) }}"><i class="fas fa-check"></i></button>
                                    <button class="btn btn-sm btn-outline-primary js-delete-cancelled"><i class="fas fa-times"></i></button>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('admin.js.delete-item')
@endsection

@extends('admin.layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mb-4">
                    <h2 class="card-header font-weight-bold">
                        {{ trans('admin.general.show') }}
                        <a class="btn btn-sm btn-outline-secondary float-right" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i> {{ trans('admin.general.back') }}</a>
                    </h2>

                    <div class="card-body">
                        @foreach($entity::getFields() as $field)
                            @if (isset($field['role']))
                                @unlessrole($field['role'])
                                    @continue
                                @endhasanyrole
                            @endif

                            @includeIf('admin.show-partials.'.$field['type'], [
                                'name' => $field['name'],
                                'label' => $field['label'],
                                'value' => $entity->{$field['name']},
                                'options' => $field['options'] ?? [],
                                'relation' => $field['relation'] ?? [],
                                'fields' => $field['fields'] ?? [],
                                'entity' => $entity,
                            ])
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.js.ckeditor')
@endsection

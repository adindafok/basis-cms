@extends('admin.layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <h2 class="card-header font-weight-bold">
                        {{ trans('admin.mediaitems.title') }}
                    </h2>

                    <div class="card-body p-0" id="ckfinder-widget">
                        @include('ckfinder::setup')

                        <script>
                            CKFinder.widget( 'ckfinder-widget', {
                                width: '100%',
                                height: 700
                            } );
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

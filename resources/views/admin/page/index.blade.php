@extends('admin.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('admin.form-partials.submit', ['classes' => 'mb-4 d-none sticky-top bg-white'])

                <div class="card mb-4">
                    <h2 class="card-header font-weight-bold">
                        {{ trans('admin.pages.title') }}
                        <a class="btn btn-sm btn-outline-success float-right" href="{{ route('admin.pages.create') }}"><i class="fas fa-plus"></i> {{ trans('admin.general.add') }}</a>
                    </h2>

                    <div class="card-body">
                        @foreach($menus as $idNumber => $menu)
                            <div class="card mb-4">
                                <div class="card-header">
                                    {{ $menu->title }}
                                </div>

                                <ul class="list-group list-group-flush list-group-item-action" id="sortableItems-{{ $idNumber }}">
                                    @foreach ($menu->menuitems as $menuitem)
                                        <li id="row-{{ $menuitem->id }}" class="list-group-item list-group-item-action" data-id="{{ $menuitem->id }}">
                                            <a href="{{ route('admin.pages.edit', ['page' => $menuitem->page]) }}">
                                                {{ $menuitem->page->title }}
                                            </a>

                                            <div class="btn-group btn-group-sm float-right" role="group" aria-label="actions">
                                                @if (empty($menuitem->page->developer_label))
                                                    <button class="btn btn-sm btn-outline-danger js-delete-button" data-id="{{ $menuitem->id }}"><i class="fas fa-trash"></i></button>
                                                @endif
                                                <a href="{{ route('admin.preview.pages', ['page' => $menuitem->page]) }}" class="btn btn-sm btn-outline-primary" target="_blank"><i class="fas fa-eye"></i></a>
                                                @if (! empty($menuitem->page->submits->count()))
                                                    <a href="{{ route('admin.pages.submits', ['page' => $menuitem->page]) }}" class="btn btn-sm btn-outline-primary"><i class="fas fa-comment-dots"></i></a>
                                                @endif
                                                <a href="{{ route('admin.pages.edit', ['page' => $menuitem->page]) }}" class="btn btn-sm btn-outline-primary"><i class="fas fa-pencil-alt"></i></a>
                                                <button class="btn btn-sm btn-outline-secondary js-order-handle"><i class="fas fa-arrows-alt"></i></button>
                                            </div>

                                            @if (empty($menuitem->page->developer_label))
                                                <div class="float-right mr-4 d-none" id="js-delete-container-{{ $menuitem->id }}" role="group" aria-label="delete-actions">
                                                    <em class="mr-4">{{ trans('admin.general.confirm-delete') }}</em>
                                                    <button class="btn btn-sm btn-outline-warning js-delete-confirmed" data-url="{{ route('admin.menuitems.destroy', ['menuitem' => $menuitem]) }}"><i class="fas fa-check"></i></button>
                                                    <button class="btn btn-sm btn-outline-primary js-delete-cancelled"><i class="fas fa-times"></i></button>
                                                </div>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>

                                @include('admin.js.sorting', [
                                    'id' => 'sortableItems-' . $idNumber,
                                    'xhr_action' => route('admin.save-order.menuitems'),
                                ])
                            </div>
                        @endforeach
                    </div>
                </div>

                @include('admin.js.delete-item')
                @include('admin.form-partials.submit', ['classes' => 'my-4 d-none'])
            </div>
        </div>
    </div>
@endsection

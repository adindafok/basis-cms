@extends('admin.layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('admin.form-partials.submit', ['classes' => 'mb-4 d-none sticky-top bg-white'])

                <div class="card">
                    <h2 class="card-header font-weight-bold">
                        {{ trans('admin.menus.title') }}
                        <a class="btn btn-sm btn-outline-success float-right" href="{{ route('admin.menus.create') }}"><i class="fas fa-plus"></i> {{ trans('admin.general.add') }}</a>
                    </h2>

                    <ul class="list-group list-group-flush list-group-item-action" id="sortableItems">
                        @foreach($items as $menu)
                            <li id="row-{{ $menu->id }}" class="list-group-item" data-id="{{ $menu->id }}">
                                <a href="{{ route('admin.menus.edit', ['menu' => $menu]) }}">
                                    {{ $menu->title }}
                                </a>

                                <div class="btn-group btn-group-sm float-right" role="group" aria-label="actions">
                                    @if (empty($menu->developer_label))
                                        <button class="btn btn-sm btn-outline-danger js-delete-button" data-id="{{ $menu->id }}"><i class="fas fa-trash"></i></button>
                                    @endif
                                    <a href="{{ route('admin.menus.edit', ['menu' => $menu]) }}" class="btn btn-sm btn-outline-primary"><i class="fas fa-pencil-alt"></i></a>
                                    <button class="btn btn-sm btn-outline-secondary js-order-handle"><i class="fas fa-arrows-alt"></i></button>
                                </div>

                                @if (empty($menu->developer_label))
                                    <div class="float-right mr-4 d-none" id="js-delete-container-{{ $menu->id }}" role="group" aria-label="delete-actions">
                                        <em class="mr-4">{{ trans('admin.general.confirm-delete') }}</em>
                                        <button class="btn btn-sm btn-outline-warning js-delete-confirmed" data-url="{{ route('admin.menus.destroy', ['menu' => $menu]) }}"><i class="fas fa-check"></i></button>
                                        <button class="btn btn-sm btn-outline-primary js-delete-cancelled"><i class="fas fa-times"></i></button>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('admin.js.delete-item')
    @include('admin.js.sorting', [
        'id' => 'sortableItems',
        'xhr_action' => route('admin.save-order.menus'),
    ])
@endsection

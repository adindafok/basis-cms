<h3>{{ $label }}</h3>

<div class="card">
    <table class="table table-striped">
        <tbody>
            @foreach (json_decode($value, true) as $keyName => $rowData)
                <tr>
                    <th>{{ $keyName }}</th>
                    <td>{{ $rowData ?? '' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

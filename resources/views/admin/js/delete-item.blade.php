<script>
    document.addEventListener('DOMContentLoaded', function(event)
    {
        var delete_buttons = document.querySelectorAll('.js-delete-button');

        for (var index = 0; index < delete_buttons.length; index++) {
            delete_buttons[index].addEventListener('click', function(event) {
                event.preventDefault();

                var id = this.dataset.id;

                var container = document.getElementById('js-delete-container-' + id);
                container.classList.remove('d-none');

                loopThroughContainerElements(id, container.children);
            });
        };

        function loopThroughContainerElements(id, elements)
        {
            for (var index = 0; index < elements.length; index++) {
                var child_element = elements[index];

                addDeleteConfirmedEvent(id, child_element);

                addCancelDeleteEvent(child_element);
            }
        }

        function addDeleteConfirmedEvent(id, button)
        {
            if (button.classList.contains('js-delete-confirmed')) {
                button.addEventListener('click', function(event) {
                    event.preventDefault();

                    makeXhrCall(id, button.dataset.url);
                });
            }
        }

        function addCancelDeleteEvent(button)
        {
            if (button.classList.contains('js-delete-cancelled')) {
                button.addEventListener('click', function(event) {
                    event.preventDefault();

                    button.parentElement.classList.add('d-none');
                });
            }
        }

        function makeXhrCall(id, url)
        {
            var token = document.querySelector('meta[name="csrf-token"]').content;

            var xhttp = new XMLHttpRequest();
            xhttp.open("DELETE", url, true);

            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.setRequestHeader('X-CSRF-TOKEN', token);

            xhttp.onreadystatechange = function() {
                if (this.readyState === 4 && this.status === 200) {
                    var row = document.getElementById('row-' + id);
                    row.parentNode.removeChild(row);
                }
            };

            xhttp.send();
        }
    });
</script>

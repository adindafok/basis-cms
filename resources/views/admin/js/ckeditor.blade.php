<script type="text/javascript" src="{{ asset('js/ckfinder/ckfinder.js') }}"></script>
<script>CKFinder.config( { connectorPath: '/ckfinder/connector' } );</script>

<script>
    document.addEventListener('DOMContentLoaded', function(event)
    {
        var editors = document.querySelectorAll('.editor');
        var token = document.querySelector('meta[name="csrf-token"]').content;

        if (typeof(editors) !== 'undefined' && editors !== null) {
            ClassicEditor
                .create(document.querySelector('.editor'), {
                    toolbar: {
                        items: [
                            'heading',
                            '|',
                            'alignment',
                            '|',
                            'bold',
                            'italic',
                            '|',
                            'link',
                            'blockquote',
                            'bulletedList',
                            'numberedList',
                            '|',
                            'insertTable',
                            '|',
                            'ckfinder',
                            'imageUpload',
                            '|',
                            'undo',
                            'redo',
                        ]
                    },
                    ckfinder: {
                        uploadUrl: '{{ route("admin.ckeditor.upload") }}?_token=' + token,
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:alignLeft',
                            'imageStyle:alignRight',
                            '|',
                            'imageTextAlternative'
                        ],
                        styles: [
                            'full',
                            'alignLeft',
                            'alignRight'
                        ],
                    },
                    table: {
                		contentToolbar: [
                			'tableColumn',
                			'tableRow',
                			'mergeTableCells'
                		]
                	},
                    language: '{{ app()->getLocale() }}'
                })
                .catch( error => {
                    console.error( error );
                });
        };

    });
</script>

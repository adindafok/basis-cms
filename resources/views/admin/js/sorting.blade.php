<script>
    document.addEventListener('DOMContentLoaded', function(event)
    {
        var sortable_items = document.getElementById('{{ $id }}');
        var save_button = document.getElementById('saveItem')

        var sortable = Sortable.create(sortable_items, {
            sort: true,
            draggable: '.list-group-item',
            handle: '.js-order-handle',
    		animation: 150,

            onEnd: function (event) {
                event.item.classList.remove('list-group-item-secondary');
            },

            onMove: function(event, originalEvent) {
                event.dragged.classList.add('list-group-item-secondary');

                save_button.classList.remove('d-none');
            },
        });

        save_button.addEventListener("click", function(e) {
            save_button.classList.add('d-none');

            saveNewOrder();
        }, false);

        function saveNewOrder()
        {
            if (sortable_items.children.length === 0) {
                return;
            }

            createOrderData(sortable_items.children);
        }

        function createOrderData(items)
        {
            var data = '?';

            for (var key = 0; key < items.length; key++) {
                data += items[key].dataset.id + '=' + key;

                if (key !== (items.length - 1) ) {
                    data += '&';
                }
            };

            makeXhrCall(data);
        }

        function makeXhrCall(params = '')
        {
            var url = '{{ $xhr_action }}' + params;

            var token = document.querySelector('meta[name="csrf-token"]').content;

            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", url, true);

            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.setRequestHeader('X-CSRF-TOKEN', token);

            xhttp.onreadystatechange = function() {
                if (this.readyState === 4 && this.status === 200) {
                    console.log(this.response);
                }
            };

            xhttp.send();
        }
    });
</script>

<script>
    document.addEventListener('DOMContentLoaded', function(event)
    {
        const editor = new EditorJs({
            holder: '{{ $name }}',

            tools: {
                header: {
                  class: Header,
                  inlineToolbar : true
                },
                table: {
                  class: Table,
                  inlineToolbar : true
                },
                link: {
                  class: Link,
                  inlineToolbar : true
                },
                paragraph: {
                  class: Paragraph,
                  inlineToolbar : true
                },
                list: {
                  class: List,
                  inlineToolbar : true
                },
                quote: {
                  class: Quote,
                  inlineToolbar : true
                },
                delimiter: {
                  class: Delimiter,
                  inlineToolbar : true
                },
            },
        });

        var save_button = document.querySelector('.btn-primary');
        save_button.addEventListener('click', function(event) {
            event.preventDefault();

            console.log('click');

            editor.save().then((outputData) => {
                console.log(outputData);
            });
        });
    });
</script>

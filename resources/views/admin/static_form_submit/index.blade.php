@extends('admin.layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('admin.form-partials.submit', ['classes' => 'mb-4 d-none sticky-top bg-white'])

                <div class="card">
                    <h2 class="card-header font-weight-bold">
                        {{ trans('admin.static_form_submits.title') }}
                        <a class="btn btn-sm btn-outline-secondary float-right" href="{{ route('admin.' . request()->segment(2) . '.index') }}"><i class="fas fa-arrow-left"></i> {{ trans('admin.general.back') }}</a>
                    </h2>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ trans('admin.static_form_submits.form_name') }}</th>
                                <th scope="col">{{ trans('admin.static_form_submits.created_at') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($entity->submits as $index => $submit)
                                <tr>
                                    <th scope="row"><a href="{{ route('admin.static_form_submits.show', ['static_form_submit' => $submit]) }}">{{ $submit->id }}</a></th>
                                    <td><a href="{{ route('admin.static_form_submits.show', ['static_form_submit' => $submit]) }}">{{ $submit->static_form->name }}</a></td>
                                    <td>{{ $submit->created_at->format('d-m-Y H:i:s') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

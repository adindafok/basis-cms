@extends('admin.layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mb-4">
                    <h2 class="card-header font-weight-bold">
                        {{ trans('admin.general.new') }}
                        <a class="btn btn-sm btn-outline-secondary float-right" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i> {{ trans('admin.general.back') }}</a>
                    </h2>

                    <div class="card-body">
                        <form action="{{ $action }}" method="POST">
                            @csrf

                            @foreach($model::getFields() as $field)
                                @if (isset($field['role']))
                                    @unlessrole($field['role'])
                                        @continue
                                    @endhasanyrole
                                @endif

                                @includeIf('admin.form-partials.'.$field['type'], [
                                    'name' => $field['name'],
                                    'label' => $field['label'] ?? '',
                                    'options' => $field['options'] ?? [],
                                    'value' => old($field['name']),
                                    'relation' => $field['relation'] ?? [],
                                    'fields' => $field['fields'] ?? [],
                                ])
                            @endforeach

                            @include('admin.form-partials.submit')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.js.ckeditor')
@endsection

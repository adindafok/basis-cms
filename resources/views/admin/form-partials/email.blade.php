<div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>
    <input type="email" class="form-control" id="{{ $name }}" name="{{ $name }}" placeholder="{{ $placeholder ?? '' }}" value="{{ $value }}">
</div>

<div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>
    <select class="form-control" name="{{ $name }}">
        @foreach($options as $optionValue => $optionName)
            @if (! empty($entity))
                <option value="{{ $optionValue }}" @if($value === $optionName) selected @endif>{{ $optionName }}</option>
            @else
                <option value="{{ $optionValue }}">{{ $optionName }}</option>
            @endif
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="{{ $name }}">{{ $label }} <em>({{ trans('admin.general.multiselect') }})</em></label>
    <select class="form-control" name="{{ $name }}" multiple>
        @foreach($options as $optionValue => $optionName)
            @if (! empty($entity))
                <option value="{{ $optionValue }}" @if(in_array($optionValue, $entity->{$relation}->pluck('id')->toArray())) selected @endif>{{ $optionName }}</option>
            @else
                <option value="{{ $optionValue }}">{{ $optionName }}</option>
            @endif
        @endforeach
    </select>
</div>

<div class="card">
    <div class="card-header">
        {{ $label }}
    </div>

    <textarea class="form-control editor" name="{{ $name }}" rows="5">{{ $value }}</textarea>
</div>

<div class="pt-4"></div>

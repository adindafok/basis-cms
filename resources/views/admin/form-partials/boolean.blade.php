<div class="form-group">
    <label>{{ $label }}</label>

    <div class="form-check form-check-inline">
        <input type="radio"
            name="{{ $name }}"
            class="form-check-input"
            id="{{ $name }}"
            value="1"
            @if($value == 1) checked @endif
        >
        <label class="form-check-label" for="{{ $name }}">{{ trans('admin.general.yes') }}</label>
    </div>

    <div class="form-check form-check-inline">
        <input type="radio"
            name="{{ $name }}"
            class="form-check-input"
            id="{{ $name }}"
            value="0"
            @if($value == 0) checked @endif
        >
        <label class="form-check-label" for="{{ $name }}">{{ trans('admin.general.no') }}</label>
    </div>
</div>

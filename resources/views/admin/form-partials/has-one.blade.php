<h3>{{ trans('admin.' . $relation . '.title') }}</h3>

@include('admin.form-partials.hidden', [
    'name' => $relation . '[id]',
    'value' => $entity->{$relation}->id ?? null,
])

@foreach($fields as $field)
    @includeIf('admin.form-partials.'.$field['type'], [
        'name' => $relation . '[' . $field['name'] . ']',
        'label' => $field['label'],
        'value' => old($relation . '[' . $field['name'] . ']') ?? $entity->{$relation}->{$field['name']} ?? '',
        'options' => $field['options'] ?? [],
        'relation' => $field['relation'] ?? [],
        'entity' => $entity->{$relation} ?? null,
    ])
@endforeach

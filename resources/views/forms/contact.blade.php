@extends('forms.base')

@section('form')
    <form action="{{ route('contact.store', ['form' => $form,]) }}" method="POST">
        @csrf
        
        <input type="hidden" name="static_formable_id" value="{{ $formable->id }}" />
        <input type="hidden" name="static_formable_type" value="{{ get_class($formable) }}" />
        <input type="hidden" name="extra_field" />

        <p>
            Naam: <input type="text" name="name" />
        </p>

        <p>
            E-mail: <input type="email" name="email" />
        </p>

        <p>Vraag:</p>
        <textarea name="content"></textarea>

        <p><button type="submit">Verzenden</button></p>
    </form>
@stop

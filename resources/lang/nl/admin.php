<?php

return [
    'general' => [
        'add' => 'Toevoegen',
        'new' => 'Nieuw',
        'edit' => 'Bewerken',
        'show' => 'Detailoverzicht',
        'back' => 'Terug',
        'submit' => 'Opslaan',
        'saving-success' => 'Opslaan gelukt',
        'deleting-success' => 'Succesvol verwijderd',
        'multiselect' => 'Houdt "ctrl" ingedrukt om meerdere opties te selecteren',
        'confirm-delete' => 'Weet je het zeker?',
        'yes' => 'Ja',
        'no' => 'Nee',
    ],

    'seeder' => [
        'menu' => [
            'main' => 'Hoofdmenu',
            'footer' => 'Footer',
            'misc' => 'Overig',
        ],

        'page' => [
            'home' => 'Home',
            'contact' => 'Contact',
        ],

        'form' => [
            'contact' => 'Contactformulier',
        ],
    ],

    'pages' => [
        'title' => 'Pagina\'s',

        'fields' => [
            'title' => 'Titel',
            'slug' => 'Slug',
            'summary' => 'Samenvatting',
            'is_published' => 'Pagina openbaar?',
            'menu_id' => 'Menu\'s',
            'developer_label' => 'Label',
            'static_forms' => 'Formulieren',
        ],
    ],

    'menus' => [
        'title' => 'Menu\'s',

        'fields' => [
            'title' => 'Titel',
            'developer_label' => 'Label',
        ],
    ],

    'redirects' => [
        'title' => 'Doorverwijzingen',

        'fields' => [
            'from' => 'Van',
            'to' => 'Naar',
        ],
    ],

    'seo' => [
        'title' => 'SEO',

        'fields' => [
            'title' => 'Meta title',
            'description' => 'Meta description',
            'index' => 'Indexering (Standaard "index, follow")',
        ],
    ],

    'static_form_submits' => [
        'title' => 'Verzonden formulieren',
        'form_name' => 'Naam van het formulier',
        'created_at' => 'Verzonden op',

        'fields' => [
            'response' => 'Ingevulde data',
        ],
    ],

    'mediaitems' => [
        'title' => 'Mediabank',

        'fields' => [
        ],
    ],
];

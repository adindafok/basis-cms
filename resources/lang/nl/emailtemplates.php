<?php

return [
    'to-owner' => [
        'error-message' => 'Het versturen uw aanvraag is niet gelukt.',
        'success-message' => 'Het versturen van uw aanvraag is gelukt.',
        'subject' => 'Er is een aanvraag binnengekomen.',

        'hello' => 'Hallo,',
        'avg' => 'Wegens AVG kunnen wij u niet direct de aanvraag naar uw mailbox mailen. We verzoeken u om via het CMS uw verzoek te bekijken.',
        'cta_text' => 'Klik hier om uw bericht te bekijken',
        'inconvience' => 'Onze excuses voor het ongemak.',
        'bye' => 'Success!', 
    ],
];
